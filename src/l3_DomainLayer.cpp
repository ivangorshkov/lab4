#include "hw/l3_DomainLayer.h"

#include <utility>

bool Bank::invariant() const {
    return !_sender->getName().empty() && _sender->getName().size() <= MAX_NAME_LENGTH
    && !_recipient->getName().empty() && _recipient->getName().size() <= MAX_NAME_LENGTH
    && _sum >= MIN_SUMM
    && !_date_of_transaction.empty() && _date_of_transaction.size() <= MAX_DATE_OF_TRANSACTION_LENGTH && _date_of_transaction.size() >= MIN_DATE_OF_TRANSACTION_LENGTH ;
}

const Account& Bank::getSender() const { return *_sender; }
const Account& Bank::getRecipient() const { return *_recipient; }

int Bank::getSum() const { return _sum; }
int Bank::getCurrencyCode() const { return _currency_code; }
int Bank::getTransactionCode() const { return _transaction_code; }
const std::string& Bank::getDate() const { return _date_of_transaction; }

bool Bank::write(std::ostream& os) {
    writeString(os, _sender->getName());
    writeString(os, _recipient->getName());
    writeNumber(os, _sum);
    writeNumber(os, _currency_code);
    writeNumber(os, _transaction_code);
    writeString(os, _date_of_transaction);
    return os.good();
}

Bank::Bank(std::shared_ptr<Account> sender,
		   std::shared_ptr<Account> recipient,
		   int summ,
		   int currency_code,
		   int transaction_code,
		   const std::string &date_of_transaction): _sender(sender), _recipient(recipient), _sum(summ), _currency_code(currency_code), _transaction_code(transaction_code), _date_of_transaction(date_of_transaction) {
  assert(invariant());
  if (summ <= sender->getMoney() && !sender->getLock() && !recipient->getLock()) {
    sender->changeMoney(-summ);
    recipient->changeMoney(summ);
  }
}

std::shared_ptr<ICollectable> ItemCollector::read(std::istream& is) {
  const std::string sender = readString(is, MAX_NAME_LENGTH);
  const std::string recipient = readString(is, MAX_NAME_LENGTH);
  int summ = readNumber<int>(is);
  int currency_code = readNumber<int>(is);
  int transaction_code = readNumber<int>(is);
  std::string date_of_transaction = readString(is, MAX_DATE_OF_TRANSACTION_LENGTH);
  return std::make_shared<Bank>(std::make_shared<Account>(sender,0), std::make_shared<Account>(recipient,0), summ, currency_code, transaction_code, date_of_transaction);
}
void ItemCollector::addAccount(std::string name, int money) {
	_accaunts.emplace_back(std::make_shared<Account>(std::move(name), money));
}
std::vector<std::shared_ptr<Account>> ItemCollector::getAccounts() {
  return _accaunts;
}
std::shared_ptr<Account> ItemCollector::getAccount(int index) {
  return _accaunts[index];
}
bool ItemCollector::saveCollection(std::string file_name) const {
  std::ofstream ofs (file_name, std::ios_base::binary);

  if (!ofs)
	return false;

  size_t count = _accaunts.size();

  writeNumber(ofs, count);

  for (const auto& acc: _accaunts) {
    writeString(ofs, acc->getName());
    writeNumber(ofs, acc->getMoney());
    writeNumber(ofs, acc->getLock());
  }

  assert(_items.size() >= _removed_count);
  count = _items.size() - _removed_count;

  writeNumber(ofs, count);

  for(size_t i=0; i < _items.size(); ++i)
	if (!_removed_signs[i])
	  _items[i]->write(ofs);

  return ofs.good();
}

bool ItemCollector::loadCollection(const std::string &file_name) {
  std::ifstream ifs (file_name, std::ios_base::binary);

  if (!ifs)
	return false;

  size_t count = readNumber<size_t>(ifs);
  _accaunts.reserve(count);
  for(size_t i=0; i < count; ++i) {
    std::string name = readString(ifs, MAX_NAME_LENGTH);
	_accaunts.emplace_back(std::make_shared<Account>(name, readNumber<int>(ifs), readNumber<bool>(ifs)));
  }
  count = readNumber<size_t>(ifs);
  _items.reserve(count);

  for(size_t i=0; i < count; ++i)
	addItem(read(ifs));

  return ifs.good();
}

Account::Account(std::string name, int money, bool lock): _name(std::move(name)), _money(money), _lock(lock) {}

std::string Account::getName() const {
  return _name;
}
int Account::getMoney() const {
  return _money;
}
void Account::changeMoney(int money) {
	_money += money;
}
void Account::changeLock(bool lock) {
	_lock = lock;
}
bool Account::getLock() const {
  return _lock;
}

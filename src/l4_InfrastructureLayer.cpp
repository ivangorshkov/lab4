#include "hw/l4_InfrastructureLayer.h"

std::string readString(std::istream& is, size_t max_string_length) {
  uint16_t len = readNumber<uint16_t>(is);
  assert(len <= max_string_length);
  char b[max_string_length+1];
  if (len > 0)
    is.read(b, len);
    
  b[len] = 0;
  return std::string(b);
}

void writeString(std::ostream& os, const std::string& s) {
    uint16_t len = s.length();
    writeNumber(os, len);
    os.write(s.c_str(), len);
}

size_t ACollector::getSize() const { return _items.size(); }

std::shared_ptr<ICollectable> ACollector::getItem(size_t index) const {
    assert(index < _items.size());
    return _items[index];
}

bool ACollector::isRemoved(size_t index) const {
    assert(index < _removed_signs.size());
    return _removed_signs[index];
}

void ACollector::addItem(const std::shared_ptr<ICollectable>& item) {
  _items.push_back(item);
  _removed_signs.emplace_back(false);
}

void ACollector::removeItem(size_t index) {
  assert(index < _items.size());
  assert(index < _removed_signs.size());
    
  if (!_removed_signs[index]) {
    _removed_signs[index] = true;
    _removed_count ++;
  }
}

void ACollector::clean() {
  _items.clear();
  _removed_signs.clear();
}

#include "hw/l2_ApplicationLayer.h"

using namespace std;

bool Application::performCommand(const vector<string> & args) {
  if (args.empty())
        return false;

  if (args[0] == "l" || args[0] == "load") {
    string filename = (args.size() == 1) ? "hw.data" : args[1];

    if (!_col.loadCollection(filename)) {
      _out.Output("Ошибка при загрузке файла '" + filename + "'");
      return false;
    }
    return true;
  }

  if (args[0] == "s" || args[0] == "save") {
    string filename = (args.size() == 1) ? "hw.data" : args[1];

    if (!_col.saveCollection(filename)) {
      _out.Output("Ошибка при сохранении файла '" + filename + "'");
      return false;
    }

    return true;
  }

  if (args[0] == "lck" || args[0] == "lock"){
	if (args.size() != 3) {
	  _out.Output("Некорректное количество аргументов команды lock");
	  return false;
	}
	std::shared_ptr<Account> account = _col.getAccount(stol(args[1]));
	bool lock = false;
	if (args[2] == "lock") {
	  lock = true;
	} else {
	  if (args[2] == "unlock") {
		lock = false;
	  } else {
		_out.Output("Некорректный аргумент команды lock");
		return false;
	  }
	}
	account->changeLock(lock);
	return true;
  }

  if (args[0] == "c" || args[0] == "clean")
    {
        if (args.size() != 1)
        {
            _out.Output("Некорректное количество аргументов команды clean");
            return false;
        }

        _col.clean();

        return true;
    }

  if (args[0] == "aa" || args[0] == "add_accaunt") {
	if (args.size() != 3)
	{
	  _out.Output("Некорректное количество аргументов команды add");
	  return false;
	}
	_col.addAccount(args[1], stoul(args[2]));
	return true;
  }

  if (args[0] == "va" || args[0] == "view_accaunts") {
	if (args.size() != 1) {
	  _out.Output("Некорректное количество аргументов команды view");
	  return false;
	}
	size_t count = 0;
	for (const auto& acc: _col.getAccounts()) {
	  _out.Output("["+std::to_string(count) + "] " + acc->getName() + " " + std::to_string(acc->getMoney()));
	  count++;
	}

	_out.Output("Количество элементов в коллекции: " + std::to_string(count));

	return true;
  }

  if (args[0] == "a" || args[0] == "add")
    {
        if (args.size() != 7)
        {
            _out.Output("Некорректное количество аргументов команды add");
            return false;
        }

        _col.addItem(make_shared<Bank>(_col.getAccount(stol(args[1])), _col.getAccount(stol(args[2])), stoul(args[3]), stoul(args[4]), stoul(args[5]), args[6].c_str()));
        return true;
    }

  if (args[0] == "r" || args[0] == "remove")
    {
        if (args.size() != 2)
        {
            _out.Output("Некорректное количество аргументов команды remove");
            return false;
        }

        _col.removeItem(stoul(args[1]));
        return true;
    }

  if (args[0] == "v" || args[0] == "view")
    {
        if (args.size() != 1)
        {
            _out.Output("Некорректное количество аргументов команды view");
            return false;
        }

        size_t count = 0;
        for(size_t i=0; i < _col.getSize(); ++i)
        {
            const Bank & item = static_cast<Bank &>(*_col.getItem(i));

            if (!_col.isRemoved(i))
            {
                _out.Output("[" + std::to_string(i) + "] "
                        + item.getSender().getName() + " "
                        + item.getRecipient().getName()  + " "
                        + std::to_string(item.getSum()) + " "
                        + std::to_string(item.getCurrencyCode()) + " "
                        + std::to_string(item.getTransactionCode()) + " "
                        + item.getDate());
                count ++;
            }
        }

        _out.Output("Количество элементов в коллекции: " + std::to_string(count));

        return true;
    }

    _out.Output("Недопустимая команда '" + args[0] + "'");
    return false;
}

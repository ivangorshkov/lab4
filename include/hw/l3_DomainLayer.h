#ifndef HW_L3_DOMAIN_LAYER_H
#define HW_L3_DOMAIN_LAYER_H

#include "hw/l4_InfrastructureLayer.h"

const size_t MAX_NAME_LENGTH    = 50;
const size_t MIN_SUMM  = 0;
const size_t MIN_DATE_OF_TRANSACTION_LENGTH  = 7;
const size_t MAX_DATE_OF_TRANSACTION_LENGTH = 11;


class Account {
 public:
  Account(std::string name, int money, bool lock=false);
  Account(std::string name);
  std::string getName() const;
  int getMoney() const;
  void changeMoney(int money);
  bool getLock() const;
  void changeLock(bool lock);

 private:
  std::string _name;
  int _money{};
  bool _lock{};
};


class Bank : public ICollectable
{
  std::shared_ptr<Account> _sender;
  std::shared_ptr<Account> _recipient;
  int _sum;
  int _currency_code;
  int _transaction_code;
  std::string _date_of_transaction;

protected:
  bool invariant() const;
public:
  Bank() = delete;
  Bank(const Bank & p) = delete;
  Bank & operator = (const Bank & p) = delete;
  Bank(std::shared_ptr<Account> sender, std::shared_ptr<Account> recipient, int summ, int currency_code, int transaction_code, const std::string& date_of_transaction);
  const Account& getSender() const;
  const Account& getRecipient() const;
  int getSum() const;
  int getCurrencyCode() const;
  int getTransactionCode() const;
  const std::string& getDate() const;
  bool write(std::ostream& os) override;
};

class ItemCollector: public ACollector
{
  std::vector<std::shared_ptr<Account>> _accaunts;
public:
  void addAccount(std::string name, int money);
  std::vector<std::shared_ptr<Account>> getAccounts();
  std::shared_ptr<Account> getAccount(int index);
  virtual std::shared_ptr<ICollectable> read(std::istream& is) override;
  bool saveCollection(const std::string file_name) const override;
  bool loadCollection(const std::string& file_name) override;
};

#endif // HW_L3_DOMAIN_LAYER_H
